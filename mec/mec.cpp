#include <iostream>
#include <vector>
#include <set>

int main() {
    int n, m;
    std::cin >> n >> m;
    int player;

    std::vector<long long> code(n);

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n/2; j++) {
            std::cin >> player;
            code[player - 1] *= 2;
            code[player - 1] ++;
        }
        for (int j = n/2; j < n; j++) {
            std::cin >> player;
            code[player - 1] *= 2;
        }
    }

    std::set<long long> codes;

    for (int i = 0; i < n; i++) {
        if (codes.find(code[i]) != codes.end()) {
            std::cout << "NIE";
            return 0;
        }

        codes.insert(code[i]);
    }

    std::cout << "TAK";

    return 0;
}
