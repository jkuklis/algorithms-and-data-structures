#include <iostream>
#include <vector>

const long long MODULO = 1000000000;
const long long MAX = 10000;

enum Side {
    LEFT, RIGHT
};

long long solve_partial(int begin, int end, Side side, int taken, std::vector<int> & sequence,
                std::vector<std::vector<long long> > & combinations) {

    if (begin == end) {
        if (side == LEFT) {
            return (sequence[begin] > taken);
        } else {
            return (sequence[begin] < taken);
        }
    }

    if (side == LEFT) {
        if (sequence[begin] > taken && sequence[end] > taken) {
            if (combinations[begin][end] == -1) {
                combinations[begin][end] = 0;
                combinations[begin][end] += solve_partial(begin + 1, end, LEFT,
                                            sequence[begin], sequence, combinations);
                combinations[begin][end] += solve_partial(begin, end - 1, RIGHT,
                                            sequence[end], sequence, combinations);

                combinations[begin][end] %= MODULO;
            }

            return combinations[begin][end];

        } else if (sequence[begin] > taken) {

            return solve_partial(begin + 1, end, LEFT, sequence[begin],
                                sequence, combinations);

        } else if (sequence[end] > taken) {

            return solve_partial(begin, end - 1, RIGHT, sequence[end],
                                sequence, combinations);

        } else {

            return 0;

        }

    } else {

        if (sequence[begin] < taken && sequence[end] < taken) {
            if (combinations[begin][end] == -1) {
                combinations[begin][end] = 0;
                combinations[begin][end] += solve_partial(begin + 1, end, LEFT,
                                            sequence[begin], sequence, combinations);
                combinations[begin][end] += solve_partial(begin, end - 1, RIGHT,
                                            sequence[end], sequence, combinations);

                combinations[begin][end] %= MODULO;
            }

            return combinations[begin][end];

        } else if (sequence[begin] < taken) {

            return solve_partial(begin + 1, end, LEFT, sequence[begin],
                                sequence, combinations);

        } else if (sequence[end] < taken) {

            return solve_partial(begin, end - 1, RIGHT, sequence[end],
                                sequence, combinations);

        } else {

            return 0;

        }
    }
}

long long solve(std::vector<int> & sequence,
                std::vector<std::vector<long long> > & combinations) {
    int n = sequence.size();

    if (n == 0)
        return 0;

    return solve_partial(0, n - 1, RIGHT, MAX, sequence, combinations);
}

int main() {
    int n;
    std::cin >> n;

    std::vector<int> sequence(n);
    std::vector<std::vector<long long> > combinations(n);

    for (int i = 0; i < n; i++) {
        combinations[i].resize(n);
    }

    for (int i = 0; i < n; i++) {
        std::cin >> sequence[i];
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            combinations[i][j] = -1;
        }
    }

    long long result = solve(sequence, combinations);

    std::cout << result;

    return 0;
}
