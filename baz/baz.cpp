#include <iostream>
#include <vector>

int main() {
    int n, m, k;
    long long even_out, odd_out;

    std::cin >> n;

    std::vector<int> prices(n);
    std::vector<long long> prices_sums(n+1);
    std::vector<int> greatest_even(n+1);
    std::vector<int> greatest_odd(n+1);
    std::vector<int> smallest_even(n+1);
    std::vector<int> smallest_odd(n+1);

    for (int i = 0; i < n; i++) {
        std::cin >> prices[i];
    }

    prices_sums[0] = 0;
    greatest_even[0] = -1;
    greatest_odd[0] = -1;
    smallest_odd[0] = -1;
    smallest_even[0] = -1;

    for (int i = 1; i <= n; i++) {
        prices_sums[i] = prices_sums[i - 1] + prices[n - i];

        if (prices[i - 1] % 2 == 0) {
            greatest_even[i] = prices[i - 1];
            greatest_odd[i] = greatest_odd[i - 1];
        } else {
            greatest_odd[i] = prices[i - 1];
            greatest_even[i] = greatest_even[i - 1];
        }

        if (prices[n - i] % 2 == 0) {
            smallest_even[i] = prices[n - i];
            smallest_odd[i] = smallest_odd[i - 1];
        } else {
            smallest_odd[i] = prices[n - i];
            smallest_even[i] = smallest_even[i - 1];
        }
    }

    std::cin >> m;

    for (int i = 0; i < m; i++) {
        std::cin >> k;
        if (prices_sums[k] % 2 == 1) {
            std::cout << prices_sums[k] << std::endl;
        } else {
            if (smallest_odd[k] == -1 || greatest_even[n - k] == -1) {
                odd_out = -1;
            } else {
                odd_out = prices_sums[k] - smallest_odd[k] + greatest_even[n - k];
            }

            if (smallest_even[k] == -1 || greatest_odd[n - k] == -1) {
                even_out = -1;
            } else {
                even_out = prices_sums[k] - smallest_even[k] + greatest_odd[n - k];
            }

            std::cout << std::max(odd_out, even_out) << std::endl;
        }
    }

    return 0;
}
