#include <iostream>

int main() {
    std::string whole;

    int current_distance = -1;
    int closest_distance = -1;

    char last_letter = '*';
    std::cin >> whole;

    for (int i = 0; i < whole.size(); i++) {
        if (whole[i] == '*') {
            current_distance ++;
        } else if (last_letter == '*' || last_letter == whole[i]) {
            last_letter = whole[i];
            current_distance = 0;
        } else {
            last_letter = whole[i];
            if (closest_distance == -1 || current_distance < closest_distance) {
                closest_distance = current_distance;
            }
            current_distance = 0;
        }
    }

    if (closest_distance == -1) {
        std::cout << "1\n";
    } else {
        std::cout << whole.size() - closest_distance << std::endl;
    }

    return 0;
}
